--------------------------------------------------------------------------------
                           HTML Import Module
--------------------------------------------------------------------------------

MAINTAINERS
-----------
Current maintainers:
 * Pratiksha dubey (pratikshad) - https://www.drupal.org/user/3158219


INTRODUCTION
------------
There are certain situations where user wold require to create a node from
external HTML files on server.

By using HTML import module process will very easy just by following the 
steps:

1) On admin page click on menu HTML Importer
   (admin/structure/html_importer/settings)
2) Once file uploaded successfully check admin/content page to see created nodes

This module create below Role and permission to manage uploaded file:
1) On admin page click on menu "HTML Importer"
2) Click on other tab Dashboard  (admin/structure/node_dashboard)


CONFIGURATION
-------------

Role : Creator

This Role will have default permission “Import HTML file” which will give a
option to administrator for form accessibility to other user.


Permission : Import HTML file

User with this permission will be able to see the form to upload zip file.
Additonally, with this permission user can manage uploaded files on File folder
via a provided dashboard page and can delete unwanted file.



Note: Make sure you have the right permissions to the directory where you want 
to upload.


INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module

 * Copy the html_importer to your modules directory and
   install the module.


RECOMMENDED MODULES
-------------------
   
 * Install html_importer module from admin panel.
 
 * Goto url admin/structure/html_importer/settings.
 
 * Provide the HTML files to create node.
    
 * Click on save.
 
 * Create a zip or tar of HTML files you want to upload.

 * Provide the path of file and upload.

 * After completion of upload your pack will be extracted to specified 
directory.


REQUIREMENTS
------------

No special requirements.
