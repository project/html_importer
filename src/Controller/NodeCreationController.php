<?php

namespace Drupal\html_importer\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Controller to Process HTML files.
 *
 * @package Drupal\html_importer.
 */
class NodeCreationController extends ControllerBase {

  /**
   * Process HTML file in batch.
   *
   * @param string $files
   *   The file path.
   * @param string $folder_uri
   *   The file extension.
   * @param string $context
   *   The batch context array.
   */
  public static function processHtmlFiles($files, $folder_uri, &$context) {
    // Calling service directly as $this remains out of the scope here.
    \Drupal::service("html_importer.batch_manager")->bathProcess($files, $folder_uri, $context);
  }

  /**
   * Finish batch to create node from HTML file.
   *
   * @param bool $success
   *   Indicate that the batch API tasks were all completed successfully.
   * @param array $results
   *   An array of all the results that were updated in processHtmlFiles().
   * @param array $operations
   *   A list of the operations that had not been completed by the batch API.
   */
  public static function finishedBatch($success, array $results, array $operations) {
    // Calling service directly as $this remains out of the scope here.
    \Drupal::service("html_importer.batch_manager")->batchFinished($success, $results, $operations);
  }

}
