<?php

namespace Drupal\html_importer\Form;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\File\FileSystem;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Process HTML file content.
 *
 * @package Drupal\html_importer.
 */
class ImportHtmlForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'htmlImporter.settings';

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityManager;

  /**
   * Drupal File System.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * Constructs HTML Form object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The EntityTypeManager service.
   * @param \Drupal\Core\File\FileSystem $file_system
   *   The File system.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, FileSystem $file_system) {
    $this->entityManager = $entity_type_manager;
    $this->fileSystem = $file_system;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('entity_type.manager'),
      $container->get('file_system')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'htmlImporter_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [
      '#attributes' => [
        'enctype' => 'multipart/form-data',
      ],
    ];
    $form['file_upload_details'] = [
      '#markup' => $this->t('Upload HTML files'),
    ];

    $validators = [
      'file_validate_extensions' => ['zip tar tar.gz'],
    ];
    $form['html_file'] = [
      '#type' => 'managed_file',
      '#name' => 'html_file',
      '#title' => $this->t('File'),
      '#size' => 20,
      '#description' => "",
      '#upload_validators' => $validators,
      '#upload_location' => 'public://html_file/',
      '#required' => TRUE,
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Zip file extract.
    $file_storage = $this->entityManager->getStorage('file');
    $file = $file_storage->load($form_state->getValue('html_file')[0]);

    // Get file extension.
    $fileArray = explode('.', $file->getFilename());
    $file_extension = end($fileArray);
    $fileName = reset($fileArray);

    $uri = file_default_scheme() . "://html_file/" . $fileName;
    $uri = \Drupal::config('system.file')->get('default_scheme') . "://html_file/" . $fileName;
    \Drupal::service('file_system')->prepareDirectory($uri, FileSystemInterface::MODIFY_PERMISSIONS);

    $extract_folder = $this->fileSystem->realpath($uri);

    if ($file) {
      $archived_file_path = $this->fileSystem->realpath($file->getFileUri());
      $achiever = $this->getCompressedFiles($archived_file_path, $file_extension);

      // Load the appropriate achiever and extract the archive.
      $achiever->extractTo($extract_folder);
    }

    // Scan directory.
    $files = \Drupal::service('file_system')->scanDirectory($uri, '/.*\\.html/');

    // Return $code;.
    $batch = [
      'init_message' => $this->t('Executing a batch...'),
      'operations' => [
        [
          ['\Drupal\html_importer\Controller\NodeCreationController',
            'processHtmlFiles',
          ],
          [$files, $uri],
        ],
      ],
      'finished' => [
        '\Drupal\html_importer\Controller\NodeCreationController',
        'finishedBatch',
      ],
    ];
    batch_set($batch);
  }

  /**
   * Extracte compressed files .
   *
   * @param string $file
   *   The file path.
   * @param string $file_extension
   *   The file extension.
   *
   * @return \PharData|\ZipArchive
   *   Return object
   */
  public function getCompressedFiles($file, $file_extension) {
    switch ($file_extension) {
      case 'tar':
        $this->compress = new \PharData($file);
        break;

      case 'zip':
        $this->compress = new \ZipArchive($file);
        $this->compress->open($file);
      default:
        break;
    }
    return $this->compress;
  }

}
