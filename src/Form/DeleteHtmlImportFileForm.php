<?php

namespace Drupal\html_importer\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Delete imported HTML files.
 *
 * @package Drupal\html_importer.
 */
class DeleteHtmlImportFileForm extends ConfirmFormBase {

  /**
   * Flag to delete file by string or int.
   *
   * @var string|int
   */
  protected $source;

  /**
   * Logger service object.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected  $loggerFactory;

  /**
   * Message service object.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected  $messenger;

  /**
   * Constructs ImportBatchManager object.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger_factory
   *   Drupal Logger Factory service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Drupal message services.
   */
  public function __construct(LoggerChannelFactory $logger_factory, MessengerInterface $messenger) {
    $this->loggerFactory = $logger_factory;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.factory'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'delete_html_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete %source ?', ['%source' => $this->source]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('nodeDashboard.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $source = '') {
    $this->source = $source;
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Load files.
    if ($file = File::load($this->source)) {
      $file->delete();
    }
    else {
      $uri = \Drupal::config('system.file')->get('default_scheme') . "://html_file/";
      $extract_details = explode('-', $this->source);
      if (strpos($uri, current($extract_details)) == FALSE) {
        $dir = file_scan_directory($uri . "/" . current($extract_details), '/.*\\.html/');
        foreach ($dir as $value) {
          if ($value->name == end($extract_details)) {
            // Delete files from folder.
            unlink($value->uri);
            break;
          }
        }
      }
    }

    // Log message for file delete.
    $this->loggerFactory->get('html_importer')->info(
      $this->t('File deleted successfully')
    );
    $this->messenger->addMessage($this->t('File deleted successfully.'));

    $form_state->setRedirect('nodeDashboard.settings');
  }

}
